import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import PostService from '../API/PostService';
import Loader from '../components/UI/Loader/Loader';
import { useFetching } from '../hooks/useFetching';

const PostIdPage = () => {
  const { id: postId } = useParams();
  const [post, setPost] = useState({});
  const [comments, setComments] = useState([]);

  // Получение данных текущего поста
  // eslint-disable-next-line no-unused-vars
  const [fetchPostById, isLoading, error] = useFetching(async (id) => {
    const response = await PostService.getById(id);
    setPost(response.data);
  });

  //Получение комментариев текущего поста
  // eslint-disable-next-line no-unused-vars
  const [fetchComments, isComLoading, comError] = useFetching(async (id) => {
    const response = await PostService.getCommentsByPostId(id);
    setComments(response.data);
  });

  useEffect(() => {
    fetchPostById(postId);
    fetchComments(postId);
  }, []);

  return (
    <div>
      <h1>Вы открыли страницу поста c ID = {post.id}</h1>
      {isLoading ? (
        <Loader />
      ) : (
        <div>
          {post.id}. {post.title}
        </div>
      )}
      {isComLoading ? (
        <Loader />
      ) : (
        <>
          <h2>Комментарии</h2>
          {comments.map((comm) => (
            <div key={comm.id} style={{ marginTop: 15 }}>
              <div>{comm.email}</div>
              <div>{comm.body}</div>
            </div>
          ))}
        </>
      )}
    </div>
  );
};

export default PostIdPage;
